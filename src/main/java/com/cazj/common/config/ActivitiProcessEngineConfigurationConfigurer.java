package com.cazj.common.config;

import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.boot.ProcessEngineConfigurationConfigurer;
import org.springframework.stereotype.Component;


import lombok.extern.slf4j.Slf4j;
/**
 *1个用于 初时activiti流程引擎的配置项， 用以将来以任何方式导入流程图时， 可以识别流程图上的中文宋体
 *@author 肖冲
 */
@Component
@Slf4j
public class ActivitiProcessEngineConfigurationConfigurer implements ProcessEngineConfigurationConfigurer{


	@Override
	public void configure(SpringProcessEngineConfiguration processEngineConfiguration) {
		processEngineConfiguration.setActivityFontName("宋体");
        processEngineConfiguration.setAnnotationFontName("宋体");
        processEngineConfiguration.setActivityFontName("宋体");

        log.info("Activiti--EngineConfigurationConfigurer配置的字体#############"+processEngineConfiguration.getActivityFontName());

		
	}

}
