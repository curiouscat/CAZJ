package com.cazj.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowNode;
import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentQuery;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.Task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cazj.common.exception.ServiceException;
import com.cazj.common.util.ShiroUtils;
import com.cazj.dao.LeaveBillDao;
import com.cazj.pojo.LeaveBill;
import com.cazj.service.WorkflowService;

import io.micrometer.core.instrument.util.StringUtils;

/**
 * 这是一个关于《工作流》处理相关的的实现接口类
 * @author 肖冲
 *
 */

@Service
public class WorkflowServiceImpl implements WorkflowService {

	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private RuntimeService runtimeService;
	@Autowired 
	private TaskService taskService;
	@Autowired
	private FormService formService;
	@Autowired
	private HistoryService historyService;

	SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");

	@Autowired
	private LeaveBillDao leaveBillDao;

	//保存新建的请假单
	@Override
	public void saveLeaveBill(LeaveBill leaveBill) {
		if(StringUtils.isEmpty(leaveBill.getUsername()))throw new IllegalArgumentException("请假人 不能为空");
		if(leaveBill.getLeaveDays()==null||leaveBill.getLeaveDays()<1)throw new IllegalArgumentException("请假天数 不能为空");
		if(leaveBill.getTypeId()==null||leaveBill.getTypeId()<1)throw new IllegalArgumentException("请假类型 不能为空");
		int rows = leaveBillDao.saveLeaveBill(leaveBill);
		if(rows<1)throw new ServiceException("保存请假单失败");

	}

	@Override
	public void updateLeaveBill(LeaveBill leaveBill) {
		if(StringUtils.isEmpty(leaveBill.getUsername()))throw new IllegalArgumentException("请假人 不能为空");
		if(leaveBill.getLeaveDays()==null||leaveBill.getLeaveDays()<1)throw new IllegalArgumentException("请假天数 不能为空");
		if(leaveBill.getTypeId()==null||leaveBill.getTypeId()<1)throw new IllegalArgumentException("请假类型 不能为空");
		int rows = leaveBillDao.updateLeaveBill(leaveBill);
		if(rows<1)throw new ServiceException("保存请假单失败");

	}

	//基于用户名查找请假列表
	@Override
	public List<LeaveBill> findLeaveBillListByUsername(String username) {
		return leaveBillDao.findLeaveBillListByUsername(username);
	}
	//查找所有请假列表
	@Override
	public List<LeaveBill> findLeaveBillList() {
		return leaveBillDao.findLeaveBillList();
	}

	//基于请假单id，删除请假单
	@Override
	public void deleteLeaveBillById(Integer workflowId) {
		if(workflowId==null||workflowId<0)throw new IllegalArgumentException("请假id参数 不能为空");
		int rows=leaveBillDao.deleteLeaveBillById(workflowId);
		if(rows<1)throw new ServiceException("删除请假单失败");
	}
	//基于请假单id，请求请假单对象
	@Override
	public LeaveBill findLeaveBillById(Integer workflowId) {
		if(workflowId==null||workflowId<0)throw new IllegalArgumentException("请假id参数 不能为空");
		LeaveBill leaveBill = leaveBillDao.findLeaveBillById(workflowId);
		return leaveBill;
	}

	@Transactional
	@Override
	public void startLeaveBillProcess(Integer workflowId) {
		if(workflowId==null||workflowId<0)throw new IllegalArgumentException("请假id参数 不能为空");
		//获取请假单id,查询请假单对象.
		LeaveBill leaveBill=leaveBillDao.findLeaveBillById(workflowId);
		//更新请假单状态state 从0-->1
		leaveBill.setState(1);
		int rows=leaveBillDao.changeLeaveBillStateById(leaveBill);
		//使用当前对象的类名,作为流程图中的key,间接启动流程
		String key=leaveBill.getClass().getSimpleName();
		//使用key启动流程实例

		/**
		 * 4：从Session中获取当前任务的办理人，使用流程变量设置下一个任务的办理人
		 * inputUser是流程变量的名称，
		 * 获取的办理人是流程变量的值
		 */
		Map<String, Object> variables = new HashMap<String,Object>();
		variables.put("inputUser", ShiroUtils.getEmpName());//表示惟一用户


		/**
		 * 5：	(1)使用流程变量设置字符串（格式：LeaveBill.id的形式），通过设置，让启动的流程（流程实例）关联业务, 这个LeaveBill表中就可以对应得上这条记录
   				(2)使用正在执行对象表中的一个字段BUSINESS_KEY（Activiti提供的一个字段），让启动的流程（流程实例）关联业务
		 */
		String businessKey = key+"."+workflowId;
		variables.put("objId", businessKey);
		runtimeService.startProcessInstanceByKey(key, businessKey, variables);

	}
	//<需我处理>的页面呈现, 需要查询的是acvitivi中,runtimeservice任务列表.
	@Override
	public List<Map<String,Object>> findTaskListByName(String assignee) {

		List<Task> list = taskService.createTaskQuery()//
				.taskAssignee(assignee)//指定个人任务查询
				.orderByTaskCreateTime().asc()//
				.list();

		List<Map<String,Object>> listMap = new ArrayList<>();

		//activiti使用很多懒加载的概念, 导致list集合回传到controller的前端时, 无法封状成json数据, 需要在这里就要把list对象转成map对象. 再回传
		for (Task task : list) {
			Map<String, Object> map=new HashMap<>();
			map.put("id", task.getId());
			map.put("name", task.getName());
			map.put("createTime", sdf.format(task.getCreateTime()));
			map.put("assignee", task.getAssignee());
			//通过任务,拿到流程定义ID,
			String processDefinitionId = task.getProcessDefinitionId();
			//通过流程定义ID,,创建流程定义查询, 获得布署对象
			ProcessDefinition defProc = repositoryService.createProcessDefinitionQuery().processDefinitionId(processDefinitionId).singleResult();
			//布署对象, 拿到部署ID
			String deploymentId = defProc.getDeploymentId();
			//部署ID,拿 到部署名称
			Deployment dep = repositoryService.createDeploymentQuery().deploymentId(deploymentId).singleResult();
			//System.out.println("deploy_name"+dep.getName());
			//将部署名称也返回到前端
			map.put("deployName", dep.getName());

			listMap.add(map);
		}

		return listMap;

	}

	//基于任务id查找FormKey
	@Override
	public String findTaskFormKeyByTaskId(String taskId) {

		TaskFormData formData = formService.getTaskFormData(taskId);
		//获取Form key的值
		String url = formData.getFormKey();
		return url;
	}


	//activi审核业务核心关联查找模块
	/**一：使用任务ID，查找请假单ID，从而获取请假单信息*/
	@Override
	public LeaveBill findLeaveBillByTaskId(String taskId) {
		//1：使用任务ID，查询任务对象Task
		Task task = taskService.createTaskQuery()//
				.taskId(taskId)//使用任务ID查询
				.singleResult();
		//2：使用任务对象Task获取流程实例ID
		String processInstanceId = task.getProcessInstanceId();
		//3：使用流程实例ID，查询正在执行的执行对象表，返回流程实例对象
		ProcessInstance pi = runtimeService.createProcessInstanceQuery()//
				.processInstanceId(processInstanceId)//使用流程实例ID查询
				.singleResult();
		//4：使用流程实例对象获取BUSINESS_KEY
		String buniness_key = pi.getBusinessKey();
		//5：获取BUSINESS_KEY对应的主键ID，使用主键ID，查询请假单对象（LeaveBill.1）
		String id = "";
		if(StringUtils.isNotBlank(buniness_key)){
			//截取字符串，取buniness_key小数点的第2个值
			id = buniness_key.split("\\.")[1];
		}else {
			throw new ServiceException("此流程没有关联buniness_key，无法反查业务类型");
		}
		//查询请假单对象
		//使用hql语句：from LeaveBill o where o.id=1
		LeaveBill leaveBill = leaveBillDao.findLeaveBillById(Integer.parseInt(id));
		return leaveBill;
	}

	/**二：已知任务ID，查询ProcessDefinitionEntiy对象，从而获取当前任务完成之后的连线名称，并放置到List<String>集合中*/
	@Override
	public List<String> findOutComeListByTaskId(String taskId) {
		//返回存放连线的名称集合
		List<String> list = new ArrayList<String>();
		//1:使用任务ID，查询任务对象
		Task task = taskService.createTaskQuery()//
				.taskId(taskId)//使用任务ID查询
				.singleResult();
		//2：获取流程定义ID
		String processDefinitionId = task.getProcessDefinitionId();
		//3：查询ProcessDefinitionEntiy对象
		ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) repositoryService.getProcessDefinition(processDefinitionId);
		//使用任务对象Task获取流程实例ID
		String processInstanceId = task.getProcessInstanceId();
		//使用流程实例ID，查询正在执行的执行对象表，返回流程实例对象
		ProcessInstance pi = runtimeService.createProcessInstanceQuery()//
				.processInstanceId(processInstanceId)//使用流程实例ID查询
				.singleResult();
		//获取当前活动的id
		String activityId = pi.getActivityId();
		
		
//		//4：获取当前的活动
//		ActivityImpl activityImpl = processDefinitionEntity.findActivity(activityId);
//		//5：获取当前活动完成之后连线的名称
//		List<PvmTransition> pvmList = activityImpl.getOutgoingTransitions();
//		if(pvmList!=null && pvmList.size()>0){
//			for(PvmTransition pvm:pvmList){
//				String name = (String) pvm.getProperty("name");
//				if(StringUtils.isNotBlank(name)){
//					list.add(name);
//				}
//				else{
//					list.add("默认提交");
//				}
//			}
//		}
		
		list.add("驳回");
		list.add("同意");
		return list;
	}

	/**三：查询所有历史审核人的审核信息，帮助当前人完成审核，返回List<Comment>*/
	//用以填充历史处理人的审核意见
	@Override
	public List<Comment> findCommentByTaskId(String taskId) {
		List<Comment> list = new ArrayList<Comment>();
		//使用当前的任务ID，查询当前流程对应的历史任务ID
		//使用当前任务ID，获取当前任务对象
		Task task = taskService.createTaskQuery()//
				.taskId(taskId)//使用任务ID查询
				.singleResult();
		//获取流程实例ID
		String processInstanceId = task.getProcessInstanceId();
//		//使用流程实例ID，查询历史任务，获取历史任务对应的每个任务ID
//		List<HistoricTaskInstance> htiList = historyService.createHistoricTaskInstanceQuery()//历史任务表查询
//						.processInstanceId(processInstanceId)//使用流程实例ID查询
//						.list();
//		//遍历集合，获取每个任务ID
//		if(htiList!=null && htiList.size()>0){
//			for(HistoricTaskInstance hti:htiList){
//				//任务ID
//				String htaskId = hti.getId();
//				//获取批注信息
//				List<Comment> taskList = taskService.getTaskComments(htaskId);//对用历史完成后的任务ID
//				list.addAll(taskList);
//			}
//		}
		list = taskService.getProcessInstanceComments(processInstanceId);
		return list;
	}
	
	
	
	//需我处理---处理流程时，提交的动作业务
	
	@Transactional
	@Override
	public void saveSubmitTask(String taskId, String outcome, String message, Integer leaveBillId) {

		//获取任务ID
		//String taskId = taskId;
		//获取连线的名称
		//String outcome = outcome;
		//批注信息
		//String message = message;
		//获取请假单ID
		Integer id = leaveBillId;
		if(outcome==null)throw new ServiceException("outcome指排动作，不能为空");
		if(!"同意".equals(outcome)&&!"驳回".equals(outcome))throw new ServiceException("outcome指排动作，只有同意和驳回2个选项");
		/**
		 * 1：在完成之前，添加一个批注信息，向act_hi_comment表中添加数据，用于记录对当前申请人的一些审核信息
		 */
		//使用任务ID，查询任务对象，获取流程流程实例ID
		Task task = taskService.createTaskQuery()//
						.taskId(taskId)//使用任务ID查询
						.singleResult();
		//获取流程实例ID
		String processInstanceId = task.getProcessInstanceId();
		/**
		 * 注意：添加批注的时候，由于Activiti底层代码是使用：
		 * 		String userId = Authentication.getAuthenticatedUserId();
			    CommentEntity comment = new CommentEntity();
			    comment.setUserId(userId);
			  所有需要从Session中获取当前登录人，作为该任务的办理人（审核人），对应act_hi_comment表中的User_ID的字段，不过不添加审核人，该字段为null
			 所以要求，添加配置执行使用Authentication.setAuthenticatedUserId();添加当前任务的审核人
		 * */
		Authentication.setAuthenticatedUserId(ShiroUtils.getEmpName());
		taskService.addComment(taskId, processInstanceId, message);
		
		/**
		 * 2：如果连线的名称是“默认提交”，那么就不需要设置，如果不是，就需要设置流程变量
		 * 在完成任务之前，设置流程变量，按照连线的名称，去完成任务
				 流程变量的名称：outcome
				 流程变量的值：连线的名称
		 */
		Map<String, Object> variables = new HashMap<String,Object>();
		//if(outcome!=null && !outcome.equals("默认提交")){

		variables.put("outcome", outcome);
		//3：使用任务ID，完成当前人的个人任务，同时流程变量
		taskService.complete(taskId, variables);
		//4：当任务完成之后，需要指定下一个任务的办理人（使用类）-----已经开发完成
		
		/**
		 * 5：在完成任务之后，判断流程是否结束
   			如果流程结束了，更新请假单表的状态从1变成2（审核中-->审核完成）
		 */
		ProcessInstance pi = runtimeService.createProcessInstanceQuery()//
						.processInstanceId(processInstanceId)//使用流程实例ID查询
						.singleResult();
		//流程结束了
		if(pi==null){
			//更新请假单表的状态从1变成2（审核中-->审核完成）
			System.out.println("流程结束了，需要把请假单改为2的状态。");
			LeaveBill bill = leaveBillDao.findLeaveBillById(id);
			bill.setState(2);
			leaveBillDao.changeLeaveBillStateById(bill);
		}
		
	}
	
	/**使用请假单ID，查询历史批注信息*/
	@Override
	public List<Comment> findCommentByLeaveBillId(Integer workflowId) {
		//使用请假单ID，查询请假单对象
		LeaveBill leaveBill = leaveBillDao.findLeaveBillById(workflowId);
		//获取对象的名称
		String objectName = leaveBill.getClass().getSimpleName();
		//组织流程表中的字段中的值
		String objId = objectName+"."+workflowId;
		
		/**1:使用历史的流程实例查询，返回历史的流程实例对象，获取流程实例ID*/
//		HistoricProcessInstance hpi = historyService.createHistoricProcessInstanceQuery()//对应历史的流程实例表
//						.processInstanceBusinessKey(objId)//使用BusinessKey字段查询
//						.singleResult();
//		//流程实例ID
//		String processInstanceId = hpi.getId();
		/**2:使用历史的流程变量查询，返回历史的流程变量的对象，获取流程实例ID*/
		HistoricVariableInstance hvi = historyService.createHistoricVariableInstanceQuery()//对应历史的流程变量表
						.variableValueEquals("objId", objId)//使用流程变量的名称和流程变量的值查询
						.singleResult();
		//流程实例ID
		String processInstanceId = hvi.getProcessInstanceId();
		List<Comment> list = taskService.getProcessInstanceComments(processInstanceId);
		return list;
	}
	
}
